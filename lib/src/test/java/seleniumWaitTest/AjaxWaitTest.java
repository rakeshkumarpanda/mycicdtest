package seleniumWaitTest;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class AjaxWaitTest 
{
	
	By emailAddressTxt = By.id("identifierId");
	By nextBtn = By.xpath("//span[text()='Next']");
	By passwordTxt = By.name("password");
	By composeButton = By.xpath("//div[text()='Compose']");
	
	
	@Test(enabled = false)
	public void waitTest() throws Exception
	{
		AjaxWait w = new AjaxWait();
		ChromeOptions options = new ChromeOptions();
		Map<String, Object> prefs = new HashMap<String, Object>();
		prefs.put("intl.accept_languages", "qps-ploc,en-us");
		options.setExperimentalOption("prefs", prefs);
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		driver.get("https://jqueryui.com/demos/");
		driver.findElement(By.xpath("//h3[text()='Widgets']/following-sibling::ul//a[text()='Accordion']")).click();
		By xpath = By.xpath("//h1[text()='Accordion']");
		w.waitTillPageLoad(driver, xpath, 60, "Accordion Page");
		String text = driver.findElement(xpath).getText();
		System.out.println(text);
		driver.quit();
		
	}
	
	@Test(enabled = true)
	public void waitTestForGmail() throws Exception
	{
		AjaxWait w = new AjaxWait();
		ChromeOptions options = new ChromeOptions();
		Map<String, Object> prefs = new HashMap<String, Object>();
		prefs.put("intl.accept_languages", "qps-ploc,en-us");
		options.setExperimentalOption("prefs", prefs);
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		driver.get("https://www.gmail.com");
//		w.waitTillPageLoad(driver, emailAddressTxt, 60, "Gmail");
//		driver.findElement(emailAddressTxt).sendKeys("");
//		driver.findElement(nextBtn).click();
//		w.waitTillPageLoad(driver, passwordTxt, 60, "Gmail");
//		driver.findElement(passwordTxt).sendKeys("");
//		driver.findElement(nextBtn).click();
//		w.waitTillPageLoad(driver, composeButton, 60, "Gmail");
//		driver.quit();
	}
}
