package seleniumWaitTest;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AjaxWait {
	
	private boolean isElementDisplayed(WebDriver driver, By by)
	{
		boolean isDisplayed = false;
		try
		{
			WebElement element = driver.findElement(by);
//			new WebDriverWait(driver, Duration.ofSeconds(1)).until(ExpectedConditions.visibilityOf(element));
			if(element.isDisplayed())
				isDisplayed = true;
		}
		catch(Exception e)
		{
			isDisplayed = false;
		}
		return isDisplayed;
	}
	
	public void waitTillPageLoad(WebDriver driver, By by, int seconds, String pageName) throws Exception
	{
		int count = 1;
		boolean isDisplayed = false;
		boolean noActive = false;
		boolean ajaxIsComplete = false;
		
		while(count<=seconds) 
		{
			ajaxIsComplete = (boolean) ((JavascriptExecutor) driver).executeScript("return document.readyState")
																		.toString()
																		.equals("complete");
			if(ajaxIsComplete) 
			{
				try
				{
					noActive = (boolean)((JavascriptExecutor) driver).executeScript("return jQuery.active==0;");
					isDisplayed = isElementDisplayed(driver, by);
				}
				catch(Exception e)
				{
					isDisplayed = isElementDisplayed(driver, by);
				}
				
				 
				if (noActive || isDisplayed)
					break;
				else
					count++;
			}
			else
				count++;
		}
		if(count>seconds)
			throw new Exception("The ajax calls for "+pageName+" could not be completed in "+seconds+" seconds");
	}

}
