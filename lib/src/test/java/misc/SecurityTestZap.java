package misc;

import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.zaproxy.clientapi.core.ApiResponse;
import org.zaproxy.clientapi.core.ClientApi;
import org.zaproxy.clientapi.core.ClientApiException;

import io.github.bonigarcia.wdm.WebDriverManager;

public class SecurityTestZap 
{
	final String ZAP_PROXY_ADDRESS = "localhost";
	final int ZAP_PROXY_PORT = 8080;
	final String ZAP_PROXY_KEY = "";
	String proxyUrl = ZAP_PROXY_ADDRESS+":"+ZAP_PROXY_PORT;
	
	WebDriver driver;
	ClientApi api;
	
	@BeforeClass
	public void init()
	{
		Proxy proxy = new Proxy();
		proxy.setHttpProxy(proxyUrl);
		proxy.setSslProxy(proxyUrl);
		
		ChromeOptions options = new ChromeOptions();
		options.setProxy(proxy);
		options.setAcceptInsecureCerts(true);
		
		driver = WebDriverManager.chromedriver().capabilities(options).create();
		api = new ClientApi(ZAP_PROXY_ADDRESS, ZAP_PROXY_PORT, ZAP_PROXY_KEY);
	}
	
	@Test
	public void secureTest()
	{
		driver.get("https://www.amazon.com");
	}
	
	@AfterClass
	public void report()
	{
		if(api!=null)
		{
			String title = "Amazon ZAP Security";
			String template = "traditional-html";
			String description = "This is the security test report";
			String reportFileName = "amazon-zap-report.html";
			String targetFolder = System.getProperty("user.dir");
			
			try{
				ApiResponse apiResponse = api.reports.generate(title, template, null, description, null, null, null, null, null, reportFileName, null
						, targetFolder, null);
				System.out.println("ZAP report generated at this location: "+apiResponse.toString());
			}catch(ClientApiException e)
			{
				e.printStackTrace();
			}
			
		}
	}

}
