package misc;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class LaunchBrowserWDM {
	
	@Test
	public void launchBrowser()
	{
		WebDriver driver = WebDriverManager.chromedriver().create();
		driver.get("http://google.com");
		System.out.println(driver.getCurrentUrl());
		System.out.println(driver.getTitle());
	}

}
