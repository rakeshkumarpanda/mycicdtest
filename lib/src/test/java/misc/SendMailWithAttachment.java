package misc;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;

public class SendMailWithAttachment {

	public static void main(String[] args) throws EmailException {
		String reportPath = System.getProperty("user.dir")+"/TestReport/ExtentReports-Version3-Test-Automaton-Report.html";
		
		EmailAttachment attachment = new EmailAttachment();
		attachment.setPath(reportPath);
		attachment.setDisposition(EmailAttachment.ATTACHMENT);
		attachment.setDescription("Automation Test Report");
		attachment.setName("Automation");

		// Create the email message
		MultiPartEmail email = new MultiPartEmail();
		email.setHostName("smtp.gmail.com");
		email.setSmtpPort(465);
		email.setAuthenticator(new DefaultAuthenticator("rakesh.panda0206@gmail.com", "Guddu@0206"));
		email.setSSLOnConnect(true);
		email.addTo("rakesh.panda0206@gmail.com");
		email.setFrom("rakesh.panda0206@gmail.com");
		email.setSubject("Automation Test Report");
		email.setMsg("Automation Execution report");

		// add the attachment
		email.attach(attachment);

		// send the email
		email.send();

	}

}
