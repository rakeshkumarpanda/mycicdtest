package misc;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.io.FileHandler;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class ScrollWithScreenshot {
	@Test
	public void takeScreenshot() throws IOException
	{
		ChromeOptions opt = new ChromeOptions();
		opt.setHeadless(true);
		WebDriver driver = WebDriverManager.chromedriver().capabilities(opt).create();
		driver.manage().window().maximize();
		driver.get("http://demo.seleniumeasy.com/javascript-alert-box-demo.html");
		
		WebElement element = driver.findElement(By.xpath("//button[text()='Click for Prompt Box']"));
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", element);
		
		File src = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileHandler.copy(src, new File(System.getProperty("user.dir")+"/HeadlessScreenshot.png"));
		
		driver.quit();
	}
}
