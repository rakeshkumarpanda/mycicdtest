//package misc;
//
//import java.util.HashMap;
//import java.util.Map;
//
//import org.openqa.selenium.By;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.chrome.ChromeOptions;
//import org.testng.annotations.Test;
//
//import io.github.bonigarcia.wdm.WebDriverManager;
//import utils.CommonEvents;
//
//public class GetValueFromField 
//{
//	By userName = By.name("username");
//	By password = By.name("password");
//	By buyNow = By.cssSelector("a[href='/broker/policy/sme/form']");
//	By quotesLink = By.xpath("//a[text()=' Quotes ']");
//	By continueBtn = By.xpath("(//a[text()='Continue'])[1]");
//	By producerNameSelectField = By.cssSelector("div#producer_name input");
//	
//	@Test
//	public void getTextFromTextBox() throws Exception
//	{
//		ChromeOptions options = new ChromeOptions();
//		Map<String, Object> prefs = new HashMap<String, Object>();
//		prefs.put("intl.accept_languages", "qps-ploc,en-us");
//		options.setExperimentalOption("prefs", prefs);
//		WebDriverManager.chromedriver().setup();
//		WebDriver driver = new ChromeDriver(options);
//		CommonEvents ce = new CommonEvents(driver);
//		driver.manage().window().maximize();
//		driver.get("https://creditcard-aiguat.democrance.com/broker/policy/sme/form/sme/QC1%2F04%2F2022%2F000094?ff=false");
//		ce.sendKeys(userName, "qa.test", "User Name", "Login page");
//		ce.sendKeys(password, "Qwerty@1234", "Password", "Login page");
//		ce.waitTillPageLoad(buyNow, 30, "Home Page");
//		ce.click(buyNow, "Buy now", "Home page");
//		ce.click(quotesLink, "quotes link", "Home page");
//		ce.waitTillElementVisible(continueBtn, 60);
//		ce.click(continueBtn, "continue btn", "quotes page");
//		ce.waitTillElementVisible(producerNameSelectField, 60);
//		String currentValue = ce.getAttribute(producerNameSelectField, "value", "producer selector", "quoted page");
//		System.out.println(currentValue);
//		Thread.sleep(5000);
////		driver.quit();
//	}
//
//}
