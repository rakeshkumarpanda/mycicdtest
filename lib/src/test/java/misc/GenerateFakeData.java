package misc;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.github.javafaker.Faker;
import com.github.javafaker.service.FakeValuesService;
import com.github.javafaker.service.RandomService;

public class GenerateFakeData {
	Faker faker;
	@BeforeClass
	public void initFaker() {
		faker = new Faker();
	}
	
	@Test
	public void generateFakeFullName() {
//		System.out.println(faker.name().fullName());
	}
	@Test
	public void generateFirstName() {
//		System.out.println(faker.name().firstName());
	}
	@Test
	public void generateLastName() {
//		System.out.println(faker.name().lastName());
	}
	@Test
	public void generateRandomNumber()
	{
//		System.out.println(faker.number().randomNumber(3, true));
//		System.out.println(faker.expression("[a-z]{3}[0-9]{2}"));
	}
	@Test
	public void generateRandom() {
		FakeValuesService fakeValuesService = new FakeValuesService(
			      new Locale("en-IN"), new RandomService());

			    String email = fakeValuesService.bothify("????##@gmail.com");
			    System.out.println(email);
			    Matcher emailMatcher = Pattern.compile("\\w{4}\\d{2}@gmail.com").matcher(email);
			 System.out.println(emailMatcher.find());
//			    assertTrue(emailMatcher.find());
	}
	

}
