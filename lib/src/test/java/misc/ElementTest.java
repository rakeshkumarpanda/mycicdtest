package misc;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

class Wait{
	public static boolean isElementPresent(WebDriver driver, By by, int time) throws InterruptedException
	{
		int count=1;
		boolean flag = false;
		while(count<=time && !flag) {
			try {
				driver.findElement(by);
				flag = true;
				break;
			}catch(Exception e) {
				Thread.sleep(1000);
				count++;
			}
		}
		return flag;
	}
	
	public static boolean elementEvaluate(WebDriver driver, String path, int time) throws InterruptedException
	{
		int count=1;
		boolean flag = false;
		while(count<=time && !flag) {
			try {
				String queryPath = "document.evaluate(\""+path+"\",document,null,XPathResult.FIRST_ORDERED_NODE_TYPE,null).singleNodeValue;";
				JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;  
				WebElement element = (WebElement) jsExecutor.executeScript(queryPath); 
				if(element.isDisplayed())
					flag=true;
			}catch(Exception e) {
				Thread.sleep(1000);
				count++;
			}
		}
		return flag;
	}
	
	public static void getElementCountFromDOM(WebDriver driver, String path) throws InterruptedException
	{
		String path1 = "var aCount = document.evaluate(\"count("+path+")\", document, null, XPathResult.ANY_TYPE, null );return aCount.numberValue;";
		System.out.println(path1);
		JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;  
		long element =(long) jsExecutor.executeScript(path1);
		System.out.println(element);
	}
	
	public static void getCountWithCss(WebDriver driver, String path)
	{
		String path1 = "return document.querySelectorAll(\""+path+"\").length;";
		System.out.println(path1);
		JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;  
		long element = Long.parseLong((String) jsExecutor.executeScript(path1).toString());
		System.out.println(element);
	}
	
}

public class ElementTest {
	@Test
	public void elementTest() throws InterruptedException
	{
//		WebDriverManager.firefoxdriver().setup();
//		WebDriver driver = new FirefoxDriver();
		WebDriver driver = WebDriverManager.firefoxdriver().create();
		
		System.out.println(driver instanceof FirefoxDriver);
		
//		driver.manage().window().maximize();
//		driver.get("http://demo.seleniumeasy.com/javascript-alert-box-demo.html");
		
//		String path="//button[text()='Click for Prompt Box']";
//		String cssPath = "*[class='panel panel-primary']";
//		By clickMePromtBox = By.xpath(path);
//		boolean isElementPresent = Wait.isElementPresent(driver, clickMePromtBox, 30);
//		if(isElementPresent) {
//			WebElement promtBoxElement = driver.findElement(clickMePromtBox);
//			promtBoxElement.click();
//		}
//		boolean evaluate = Wait.elementEvaluate(driver, path, 30);
//		System.out.println(evaluate);
//		Wait.getElementCountFromDOM(driver, path);
//		Wait.getCountWithCss(driver, cssPath);
			
		
		
		Thread.sleep(5000);
		driver.quit();
	}
}
