package browserLaunching;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class ChromePath 
{
	@Test
	public void chromePath()
	{
		String exePath = System.getProperty("user.dir")+"/src/main/resources/chromedriver";
		System.setProperty("webdriver.chrome.driver", exePath);
		WebDriver driver = new ChromeDriver();
		driver.get("http://www.google.com");
	}

}
