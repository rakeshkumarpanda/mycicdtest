package testTestNG;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import io.github.bonigarcia.wdm.WebDriverManager;

public class BaseTest 
{
	protected WebDriver driver;
	
	@BeforeClass(alwaysRun = true)
	public void initBrowser() throws MalformedURLException
	{
//		FirefoxOptions options = new FirefoxOptions();
//		options.setPlatformName("Linux");
//		options.setBrowserVersion("99.0");
//		options.addPreference("intl.accept_languages", "qps-ploc,en-us");
//		String completeUrl = "http://localhost:4444/wd/hub";
//		driver = new RemoteWebDriver(new URL(completeUrl), options);
//		driver = WebDriverManager.firefoxdriver().capabilities(options).create();
		driver = WebDriverManager.chromedriver().create();
		driver.get("https://www.gmail.com");
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown()
	{
		driver.quit();
	}

}
