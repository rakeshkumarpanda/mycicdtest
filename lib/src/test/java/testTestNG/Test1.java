package testTestNG;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Test1
{
	private WebDriver driver;
	@BeforeClass(alwaysRun = true)
	public void initBrowser()
	{
//		WebDriverManager.chromedriver().setup();
		FirefoxOptions options = new FirefoxOptions();
		options.setHeadless(true);
		driver = WebDriverManager.firefoxdriver().capabilities(options).create();
	}
	
	
	@Test(groups = {"Test1Regression"})
	public void aTest1()
	{
//		String chromeDriverPath = System.getProperty("user.dir")+"/src/main/resources/chromedriver.exe";
//		System.setProperty("webdriver.chrome.driver", chromeDriverPath);
//		WebDriver driver = WebDriverManager.chromedriver().create();
//		driver.get("https://www.google.com");
		System.out.println(driver.getTitle());
		System.out.println("aTest1");
	}
	
	@Test(groups = {"TestSanity", "Regression"})
	public void bTest1()
	{
//		System.out.println(driver.getCurrentUrl());
		System.out.println("bTest1");
	}
	
	@Test(groups = {"Test1", "Regression"})
	public void cTest1()
	{
		System.out.println("cTest1");
	}
	
	@Test(groups = {"Test1", "Sanity", "Regression"})
	public void dTest1()
	{
		System.out.println("dTest1");
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown()
	{
		driver.quit();
	}

}
