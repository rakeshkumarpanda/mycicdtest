package testTestNG;

import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

public class Test2
{
	private WebDriver driver;
	
	@BeforeClass(alwaysRun = true)
	public void initBrowser()
	{
		ChromeOptions options = new ChromeOptions();
		options.setHeadless(true);
//		try {
//			options.setCapability("platformName", "Linux");
//			options.setCapability("browserVersion", "101.0");
//			options.setHeadless(true);
//			options.setCapability("intl.accept_languages", "qps-ploc,en-us");
//			Map<String, Object> prefs = new HashMap<String, Object>();
//			prefs.put("intl.accept_languages", "qps-ploc,en-us");
//			options.setExperimentalOption("prefs", prefs);
//			WebDriverManager.chromedriver().setup();
//			MutableCapabilities option = new ChromeOptions();
//			option.setCapability("browserVersion", "101.0");
//			option.setCapability("browserName", Browser.CHROME);
//			option.setCapability("platformName", Platform.LINUX);
//			String testName = ctx.getCurrentXmlTest().getName();
//			System.out.println(testName);
//			option.setCapability("name", testName);
//			String completeUrl = "http://selenium__standalone-chrome:4444/wd/hub";
//			driver = new RemoteWebDriver(new URL(completeUrl), options);
//		}catch(Exception e) {
//			e.printStackTrace();
//		}
		
//		WebDriverManager.chromedriver().setup();
//		driver = new ChromeDriver(options);
//		driver = WebDriverManager.chromedriver().capabilities(options).create();
//		driver = WebDriverManager.chromedriver().create();
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/src/main/resources/chromedriver");
		driver = new ChromeDriver(options);
		driver.get("https://www.gmail.com");
	}
	
	@Test(groups = {"Test2", "Sanity", "Regression"})
	public void eTest1()
	{
		System.out.println("eTest1");
	}
	
	@Test(groups = {"TestSanity", "Regression"})
	public void fTest1()
	{
		System.out.println("fTest1");
	}
	
	@Test(groups = {"Test2", "Sanity", "Regression"})
	public void gTest1()
	{
		System.out.println("gTest1");
	}
	
	@Test(groups = {"Test2", "Sanity", "Regression"})
	public void hTest1()
	{
		System.out.println("hTest1");
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown()
	{
		driver.quit();
	}

}
