package testCap;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.testng.annotations.Test;

public class ReadNestedJSON 
{
	@Test
	public void readJson()
	{
		String jsonFilePath = "/Users/rakesh/SessionWorkspace/Test/lib/src/main/resources/TestData/testdata.json";
		JSONParser parser = new JSONParser();
		List<String> l = new ArrayList<String>();
		
		try
		{
			JSONObject jsonObject = (JSONObject) parser.parse(new FileReader(jsonFilePath));
			JSONArray array = (JSONArray)jsonObject.get("input");
			System.out.println(array.size());
//			for(int i=0; i<array.size(); i++)
//			{
//				JSONObject o1 = (JSONObject) (array.get(i));
////				System.out.println(o1.get("firstname"));
//				JSONArray a1 = (JSONArray)(o1.get("Test Array"));
//				System.out.println(a1.size());
//			}
//			Iterator itr = array.iterator();
//			while(itr.hasNext())
//			{
//				JSONObject o1 = (JSONObject) (itr.next());
//				System.out.println(o1.get("firstname"));
//				JSONArray a1 = (JSONArray)(o1.get("Test Array"));
//				System.out.println(a1.size());
//			}
			array.parallelStream().forEach(s->l.add((String)((JSONObject) (s)).get("firstname")));
			l.parallelStream().forEach(s->System.out.println(s));
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}

}
