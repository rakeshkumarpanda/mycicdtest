package testCap;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.jayway.jsonpath.JsonPath;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;


class TestJSONFile
{
	public Iterator<Object> collectData(File file, String path) throws IOException
	{
//		int size = JsonPath.read(file, path+".length()");
//		System.out.println(size);
		JSONArray arr=(JSONArray)(JsonPath.read(file, path));
		return arr.iterator();
	}
}

public class ReadJSONUsingJSONPath {

	public static void main(String[] args) throws IOException {
		String jsonFilePath = "/Users/rakesh/SessionWorkspace/Test/lib/src/main/resources/TestData/testdata.json";
		File jsonFile = new File(jsonFilePath);
		String path = "$.input";
//		int totalInputs = JsonPath.read(jsonFile, "$.input.length()");
//		System.out.println(totalInputs);
//		JSONArray ja = (JSONArray)(JsonPath.read(jsonFile, "$.input[0].testarray"));
//		System.out.println(ja.size());
//		Map<String, String> m = JsonPath.read(jsonFile, "$.input");
//		System.out.println(m.get("testvalues"));
		Map<String, String> m1 = JsonPath.read(jsonFile, "$.input[0].testarray[1]");
		System.out.println(m1.get("Value1"));
		
		int testArraySize = JsonPath.read(jsonFile, "$.input[0].testarray.length()");
		System.out.println(testArraySize);
	}

}
