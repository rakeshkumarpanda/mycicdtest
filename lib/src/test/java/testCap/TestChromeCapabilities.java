package testCap;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class TestChromeCapabilities {

	public static void main(String[] args) throws InterruptedException {
		ChromeOptions options = new ChromeOptions();
		Map<String, Object> prefs = new HashMap<String, Object>();

		prefs.put("intl.accept_languages", "qps-ploc,en-us");

		options.setExperimentalOption("prefs", prefs);
//		options.addArguments("--lang=es");
//		Set<String> capabilityNames = options.getCapabilityNames();
//		capabilityNames.parallelStream().forEach(s->System.out.println(s));
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver(options);
		driver.get("https://www.gmail.com");
		
		Thread.sleep(5000);
		driver.quit();
	}

}
