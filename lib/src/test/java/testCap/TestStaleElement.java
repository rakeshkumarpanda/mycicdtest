package testCap;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import io.github.bonigarcia.wdm.WebDriverManager;

class CountClass
{
	static int count=1;
}


class StaleTest
{
	By firstName = By.name("first_name");
	public void testStale(WebDriver driver)
	{
		try
		{
			WebElement element = driver.findElement(firstName);
			element.sendKeys("Test");
			driver.navigate().refresh();
			element.sendKeys("Selenium");
		}
		catch(StaleElementReferenceException e)
		{
			CountClass.count++;
			if(CountClass.count<3)
			{
				driver.navigate().refresh();
				new StaleTest().testStale(driver);
			}
				
		}
	}
}

public class TestStaleElement {

	public static void main(String[] args) throws InterruptedException 
	{
		StaleTest s = new StaleTest();
		ChromeOptions options = new ChromeOptions();
		Map<String, Object> prefs = new HashMap<String, Object>();
		prefs.put("intl.accept_languages", "qps-ploc,en-us");
		options.setExperimentalOption("prefs", prefs);
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver(options);
		driver.get("https://demo.seleniumeasy.com/input-form-demo.html");
		s.testStale(driver);
		Thread.sleep(5000);
		driver.quit();
	}

}
